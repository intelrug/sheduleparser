const XLSX = require('xlsx');
const request = require('request');

const TYPE_LECTURE = 1;
const TYPE_PRACT = 2;
const TYPE_LAB = 3;

const auditoryRegExp = /^([А-ЯA-Z] [0-9]+)$|^([0-9]+)$|^([А-ЯA-Z] [0-9]+[a-яa-z])$|^([0-9]+[a-яa-z])$/;


class Parser {

    constructor() {
        this.sheet = null;
        this.ranges = null;

        this.getWorkbook();
    }

    getWorkbook() {
        request("http://www.vstu.ru/upload/raspisanie/z/%D0%9E%D0%9D_%D0%A4%D0%AD%D0%92%D0%A2_2%20%D0%BA%D1%83%D1%80%D1%81.xls", { encoding: null }, (err, res, data) => {
            if (err || res.statusCode !== 200) return;

            const workbook = XLSX.read(data, { type: 'buffer' });
            this.sheet = workbook.Sheets[workbook.SheetNames[0]];
            this.ranges = this.sheet['!merges'];
            this.parse();
        });
    }

    getCellCoords(col, row) {
        let index = this.ranges.findIndex(range => range.s.c <= col && range.e.c >= col && range.s.r === row);
        if (index === -1) {
            return null;
        }
        return this.ranges[index];
    }

    getName() {

    }

    getType(coords) {
        if (coords.e.c - coords.s.c === 3) {

            let nextLessonTeachersCount = 0;
            let teachersCount = 0;

            let checkCoords = { c: coords.s.c, r: coords.s.r + 3 };
            let cellRef = Parser.getRef(checkCoords);
            let content = this.sheet[cellRef];
            const nextLessonHasName = !!content;

            for (let i = coords.s.r + 1; i <= coords.s.r + 2; ++i) {
                let checkCoords = { c: coords.s.c, r: i };
                let nextLessonCheckCoords = { c: coords.s.c, r: i + 3 };
                let cellRef = Parser.getRef(checkCoords);
                let nextLessonCellRef = Parser.getRef(nextLessonCheckCoords);
                let content = this.sheet[cellRef];
                let nextLessonContent = this.sheet[nextLessonCellRef];
                if (content) {
                    teachersCount += 1;
                }
                if (nextLessonContent) {
                    nextLessonTeachersCount += 1;
                }
            }

            if (
                (nextLessonHasName && teachersCount > 1)
                || (!nextLessonHasName && teachersCount > 1)
                || (!nextLessonHasName && nextLessonTeachersCount > 0)
            ) {
                return TYPE_LAB;
            }

            return TYPE_PRACT;
        } else if (coords.e.c - coords.s.c > 3) {
            return TYPE_LECTURE;
        } else if (coords.e.c - coords.s.c === 1) {
            return TYPE_LAB;
        }
    }

    getTeacher() {

    }

    getAuditory() {

    }

    static getRef(coords) {
        return XLSX.utils.encode_cell(coords);
    }

    parse() {
        const coords = {
            s: { c: 19, r: 6 },
            e: { c: 22, r: 222 }
        };
        const result = [];

        for (let i = 0; i < 12; ++i) {
            for (let r = coords.s.r + 18 * i + ((i >= 6) ? 1 : 0); r < coords.s.r + 18 * (i + 1) && r < coords.e.r; r += 3) {
                let nameCoords = this.getCellCoords(coords.s.c, r);

                if (nameCoords != null) {
                    const ref = Parser.getRef(nameCoords.s);
                    const name = this.sheet[ref];

                    if (!auditoryRegExp.test(String(name.v))) {
                        const lesson = {
                            name: name ? name.v : null,
                            type: this.getType(nameCoords)
                            //teacher: teacher ? teacher.v : null,
                            //auditory: auditory ? auditory.v : null
                        };

                        result.push(lesson);
                        console.log(lesson);
                    }
                }

                nameCoords = this.getCellCoords(coords.e.c, r);
                if (nameCoords != null) {
                    if (nameCoords.e.c - nameCoords.s.c > 1)
                        continue;

                    const ref = Parser.getRef(nameCoords.s);
                    const name = this.sheet[ref];

                    if (auditoryRegExp.test(String(name.v)))
                        continue;

                    const lesson = {
                        name: name ? name.v : null,
                        type: this.getType(nameCoords)
                        //teacher: teacher ? teacher.v : null,
                        //auditory: auditory ? auditory.v : null
                    };

                    result.push(lesson);
                    console.log(lesson);
                }
            }
        }
    }
}

new Parser();